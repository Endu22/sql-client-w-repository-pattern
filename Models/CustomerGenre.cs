﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Client_W_Repository_Pattern.Models
{
    public class CustomerGenre
    {
        public string? Name { get; set; }
        public int NumOfPurchasedInGenre { get; set; }
    }
}
