﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Client_W_Repository_Pattern.Models
{
    public class CustomerCountry
    {
        public string? Country { get; set; }
        public int NumOfCustomers { get; set; }

    }
}
