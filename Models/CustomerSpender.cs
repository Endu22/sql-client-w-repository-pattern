﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Client_W_Repository_Pattern.Models
{
    public class CustomerSpender
    {
        public int CustomerId { get; set; }
        public decimal Total { get; set; }

    }
}
