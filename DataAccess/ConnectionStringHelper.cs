﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Client_W_Repository_Pattern.DataAccess
{
    public class ConnectionStringHelper
    {
        /// <summary>
        /// A method used to establish connection to the database. Be sure to edit the DataSource value in the connection strings with your own.
        /// </summary>
        /// <returns>A string used to connect to the databse.</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = "N-SE-01-7199\\SQLEXPRESS"; //replace N-SE-01-9952\\SQLEXPRESS with personal instance of SQLEXPRESS name
            connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
            connectionStringBuilder.Encrypt = false;
            return connectionStringBuilder.ConnectionString;
        }
    }
}
