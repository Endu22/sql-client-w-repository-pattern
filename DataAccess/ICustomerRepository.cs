using SQL_Client_W_Repository_Pattern.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Client_W_Repository_Pattern.DataAccess
{
    public interface ICustomerRepository
    {
        public Customer GetCustomerById(int id);
        public Customer GetCustomerByName(string name);
        public List<Customer> GetAllCustomers();
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public List<CustomerGenre> GetACustomersFavoriteGenre(int id);
        public List<Customer> GetPageOfCustomers(int offset, int fetchNext);
        public List<CustomerCountry> GetAllCustomerCountries();
        public List<CustomerSpender> GetHighestSpenders();
    }
}
