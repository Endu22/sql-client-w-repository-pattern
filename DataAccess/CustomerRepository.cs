﻿using Microsoft.Data.SqlClient;
using SQL_Client_W_Repository_Pattern.Models;

namespace SQL_Client_W_Repository_Pattern.DataAccess
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Method to read all the customers from the database.
        /// </summary>
        /// <returns>A list of all customers</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "NULL" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "NULL" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return customerList;
        }

        /// <summary>
        /// A method to read a specific customer from the database by Id.
        /// </summary>
        /// <param name="id">The id of a customer</param>
        /// <returns>A customer with the given Id</returns>
        public Customer GetCustomerById(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @CustomerId";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Reader
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? "NULL" : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? "NULL" : reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return customer;
        }

        /// <summary>
        /// Read a specific customer from the database by name. On partial searches it returns the first customer appearing in alphabetical order.  
        /// </summary>
        /// <param name="name">The first name of a customer</param>
        /// <returns>A customer with the given name</returns>
        public Customer GetCustomerByName(string name)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE Firstname LIKE @firstName ORDER BY FirstName";
            try
            {
                // connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Create sql command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@firstName", name + "%");
                        //Read
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            reader.Read();
                            customer.CustomerId = reader.GetInt32(0);
                            customer.FirstName = reader.GetString(1);
                            customer.LastName = reader.GetString(2);
                            customer.Country = reader.GetString(3);
                            customer.PostalCode = reader.IsDBNull(4) ? "NULL" : reader.GetString(4);
                            customer.Phone = reader.IsDBNull(5) ? "NULL" : reader.GetString(5);
                            customer.Email = reader.GetString(6);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customer;
        }

        /// <summary>
        /// A method that returns a page of customers starting from given <paramref name="offset"/> until the specified limit <paramref name="fetchNext"/>.
        /// </summary>
        /// <param name="offset">Start of page</param>
        /// <param name="fetchNext">Limit of rows</param>
        /// <returns>A list of customers</returns>
        public List<Customer> GetPageOfCustomers(int offset, int fetchNext)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                "ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @FetchNext ROWS ONLY";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Reader
                        cmd.Parameters.AddWithValue("@Offset", offset);
                        cmd.Parameters.AddWithValue("@FetchNext", fetchNext);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "NULL" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "NULL" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return customerList;
        }

        /// <summary>
        /// A method that adds a new customer to the database. 
        /// </summary>
        /// <param name="customer">The customer to be added</param>
        /// <returns>A boolean. Returns true if there was a successful addition. Else false.</returns>
        public bool AddNewCustomer(Customer customer)
        {
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(@firstName, @lastName, @country, @postalCode, @phone, @email)";
            try
            {
                // connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Create sql command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@firstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@lastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@country", customer.Country);
                        cmd.Parameters.AddWithValue("@postalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@email", customer.Email);
                        int rowsAffected = cmd.ExecuteNonQuery();

                        if (rowsAffected == 1)
                            return true;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return false;
        }

        /// <summary>
        /// A method to update an existing customer.
        /// </summary>
        /// <param name="customer">The customer to be updated</param>
        /// <returns>A boolean. True if the update is successful. Else false</returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer " +
                "SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return success;
        }

        /// <summary>
        /// A method to return the number of customers in each country in descending order.
        /// </summary>
        /// <returns>A list of customer countries.</returns>
        public List<CustomerCountry> GetAllCustomerCountries()
        {
            List<CustomerCountry> customerCountryList = new List<CustomerCountry>();
            string sql = "SELECT Country, COUNT(CustomerID) FROM Customer GROUP BY Country ORDER BY COUNT(CustomerID) DESC";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                CustomerCountry customerCountry = new CustomerCountry();
                                customerCountry.Country = reader.GetString(0);
                                customerCountry.NumOfCustomers = reader.GetInt32(1);
                                customerCountryList.Add(customerCountry);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return customerCountryList;
        }

        /// <summary>
        /// A method that returns the 10 highest spenders from all customers in descending order.
        /// </summary>
        /// <returns>A list of the highest customer spenders</returns>
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> customerList = new List<CustomerSpender>();
            string sql = "SELECT Customer.CustomerId, SUM(Invoice.Total) as Total  FROM Customer INNER JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId GROUP BY Customer.CustomerId ORDER BY Total DESC OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                CustomerSpender customer = new CustomerSpender();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.Total = reader.GetDecimal(1);
                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return customerList;
        }

        /// <summary>
        /// A method that returns the favorite genre(s) of a given customer.
        /// If a customer has two favorite genres that they bought equal times then both are returned. 
        /// Else the most popular.
        /// </summary>
        /// <param name="customerId">The Id of a customer</param>
        /// <returns>A list containing one or two genres.</returns>
        public List<CustomerGenre> GetACustomersFavoriteGenre(int customerId)
        {
            List<CustomerGenre> customerGenreList = new List<CustomerGenre>();
            string sql = "SELECT Genre.Name, COUNT(Genre.Name) FROM Customer " +
                        "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                        "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                        "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                        "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                        "WHERE Customer.CustomerId = @CustomerId " +
                        "GROUP BY Genre.Name " +
                        "ORDER BY COUNT(Genre.Name) DESC " +
                        "OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Create sql command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customerId);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre customerGenre = new CustomerGenre();
                                customerGenre.Name = reader.GetString(0);
                                customerGenre.NumOfPurchasedInGenre = reader.GetInt32(1);
                                customerGenreList.Add(customerGenre);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            // Check if the two customerGenres fetched from db is not equal -> return a list containing the most common genre. Else return a list containing both.
            if (customerGenreList[0].NumOfPurchasedInGenre > customerGenreList[1].NumOfPurchasedInGenre)
            {
                // Return list with the most popular
                List<CustomerGenre> listToReturn = new List<CustomerGenre>();
                listToReturn.Add(customerGenreList[0]);
                return listToReturn;
            }

            return customerGenreList;
        }
    }
}
