# Assignment 5 - Create a database and access it.

## Acknowledgments
This project was created as a solution during the Experis Bootcamp (2022) with Noroff Education AS.

## Install
Clone the repository. The scripts for Appendix A part are located in the `SqlScriptsAppendixA` folder in order.
The rest are part of Appendix B. In order for it to run you would need to change to your personal SQLSERVEREXPRESS name in the `ConnectionStringHelper` file like shown below:
 ```
public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = "N-SE-01-9952\\SQLEXPRESS"; 
            connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
            connectionStringBuilder.Encrypt = false;
            return connectionStringBuilder.ConnectionString;
        }
```
Replace : ``N-SE-01-9952\\SQLEXPRESS`` with your own Microsoft SQL Server name (do not forget to include the additional backslash). 

## Usage
The purpose of this console application was to manage the Chinook database (provided by Noroff Education AS). All the requirements are printed in the console when run. 

## Dependencies
Visual Studio Community 22 (.NET 6),  Windows 10 and the Microsoft.Data.SqlClient (5.0.0) package from NuGet.

## Contributors
Oliver Rimmi - oliver.rimmi@se.experis.com <br>
Sofia Vulgari - sofia.vulgari@se.experis.com 
