USE SuperheroesDb

INSERT INTO Superhero(Name, Alias, Origin)
VALUES('Peter Parker', 'Spiderman', 'New York')

INSERT INTO Superhero(Name, Alias, Origin)
VALUES('Wanda Maximoff', 'Scarlet Witch', 'Sokovia')

INSERT INTO Superhero(Name, Alias, Origin)
VALUES('T''Challa', 'Black Panther', 'Wakanda')