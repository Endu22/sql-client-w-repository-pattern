USE SuperheroesDb

INSERT INTO Power(Name, Description)
VALUES('Strength', 'Superhuman strength'),
('Superhuman- senses', 'Tingly and eerie intuitions'),
('Magic', 'Power to influence events or things with supernatural forces'),
('Wall crawling', 'Can climb on walls')

INSERT INTO SuperheroPowerLink(Superhero_id, Power_id)
VALUES(1, 1), (1,2), (1,4), (2,3), (3,1), (3,2);