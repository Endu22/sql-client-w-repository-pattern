USE SuperheroesDb

CREATE TABLE Superhero(
    ID int PRIMARY KEY IDENTITY(1,1),
    Name nvarchar(30) null,
    Alias nvarchar(30) null,
    Origin nvarchar(30) null
)

CREATE TABLE Assistant(
    ID int PRIMARY KEY IDENTITY(1,1),
    Name nvarchar(30),
)

CREATE TABLE Power(
    ID int PRIMARY KEY IDENTITY(1,1),
    Name nvarchar(30),
    Description nvarchar(300)
)