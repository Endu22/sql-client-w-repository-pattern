USE SuperheroesDb

CREATE TABLE SuperheroPowerLink(
	Superhero_id int not null,
	Power_id int not null,
	CONSTRAINT FK_Superhero FOREIGN KEY (Superhero_id) REFERENCES Superhero(ID),
	CONSTRAINT FK_Power FOREIGN KEY (Power_id) REFERENCES Power(ID),

	CONSTRAINT pk_linkConstraint PRIMARY KEY (Superhero_id, Power_id)
)