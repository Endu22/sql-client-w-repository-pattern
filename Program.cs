﻿using SQL_Client_W_Repository_Pattern.Models;
using SQL_Client_W_Repository_Pattern.DataAccess;

namespace SQL_Client_W_Repository_Pattern
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            Console.WriteLine("Requirement 1: " + "TestSelectAll()");
            TestSelectAll(repository);
            Console.WriteLine("Requirement 2: " + "TestSelectById()");
            TestSelectById(repository);
            Console.WriteLine("Requirement 3: " + "TestSelectByName()");
            TestSelectByName(repository);
            Console.WriteLine("Requirement 4: " + "TestSelectPageOfCustomers()");
            TestSelectPageOfCustomers(repository);
            Console.WriteLine("Requirement 5: " + "TestAddCustomer()");
            TestAddCustomer(repository);
            Console.WriteLine("Requirement 6: " + "TestUpdateCustomer()");
            TestUpdateCustomer(repository);
            Console.WriteLine("Requirement 7: " + "TestGetAllCustomerCountries()");
            TestGetAllCustomerCountries(repository);
            Console.WriteLine("Requirement 8: " + "PrintHighestPayingCustomers()");
            PrintHighestPayingCustomers(repository);
            Console.WriteLine("Requirement 9: " + "TestFavoriteGenre()");
            TestFavoriteGenre(repository);
        }

        public static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        public static void TestSelectById(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerById(1));
        }

        public static void TestSelectByName(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerByName("L"));
        }

        static void TestAddCustomer(ICustomerRepository repository)
        {
            Customer testCustomer = new Customer()
            {
                FirstName = "John",
                LastName = "Doe",
                Country = "United Kingdom",
                PostalCode = "SW2R 2EK",
                Phone = "+44 012 3456 4567",
                Email = "john.doe@gmail.com"
            };

            if (repository.AddNewCustomer(testCustomer))
            {
                Console.WriteLine("Insert successfull!");
            }
            else
            {
                Console.WriteLine("Insert unsuccessfull...");
            }
        }

        static void TestUpdateCustomer(ICustomerRepository repository)
        {
            Customer testCustomer = repository.GetCustomerById(20);
            testCustomer.FirstName = "John";
            testCustomer.LastName = "Doe";
            testCustomer.Country = "United Kingdom";
            testCustomer.PostalCode = "SW2R 2EK";
            testCustomer.Phone = "+44 012 3456 4567";
            testCustomer.Email = "john.doe@gmail.com";

            if (repository.UpdateCustomer(testCustomer))
            {
                Console.WriteLine("Update successfull!");
            }
            else
            {
                Console.WriteLine("Update unsuccessfull...");
            }
        }

        public static void TestSelectPageOfCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetPageOfCustomers(15, 15));
        }


        public static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        public static void PrintHighestCustomers(IEnumerable<CustomerSpender> invoices)
        {
            foreach (CustomerSpender invoice in invoices)
            {
                PrintInvoices(invoice);
            }
        }

        public static void PrintHighestPayingCustomers(ICustomerRepository repository)
        {
            PrintHighestCustomers(repository.GetHighestSpenders());
        }

        public static void PrintInvoices(CustomerSpender invoice)
        {
            Console.WriteLine($"--- ID: {invoice.CustomerId} Total: {invoice.Total} ---");
        }


        public static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- {customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email} ---");
        }

        #region CustomerCountry tests
        static void TestGetAllCustomerCountries(ICustomerRepository repository)
        {
            PrintCustomerCountries(repository.GetAllCustomerCountries());
        }

        public static void PrintCustomerCountries(IEnumerable<CustomerCountry> customerCountries)
        {
            foreach (CustomerCountry customerCountry in customerCountries)
            {
                PrintCustomerCountry(customerCountry);
            }
        }

        public static void PrintCustomerCountry(CustomerCountry customerCountry)
        {
            Console.WriteLine($"--- {customerCountry.Country} - {customerCountry.NumOfCustomers} ---");
        }
        #endregion

        #region CustomerGenre tests
        public static void TestFavoriteGenre(ICustomerRepository repository)
        {
            List<CustomerGenre> list = repository.GetACustomersFavoriteGenre(12);
            foreach (CustomerGenre customerGenre in list)
            {
                Console.WriteLine($"--- Genre: {customerGenre.Name} - Nr of Purchases: {customerGenre.NumOfPurchasedInGenre} ---");
            }
        }

        #endregion
    }
}